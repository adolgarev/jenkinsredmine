package co.mediasapiens.jenkinsredmine;

import hudson.Extension;
import hudson.Launcher;
import hudson.Launcher.ProcStarter;
import hudson.Proc;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.BuildListener;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.Builder;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kohsuke.stapler.DataBoundConstructor;

/**
 *
 * @author adolgarev
 */
public class JenkinsRedmineBuilder extends Builder {

    private final String redmineUrl;
    private final String issuesUrl;
    private final Integer issueFixNumberFieldId;

    // Fields in config.jelly must match the parameter names in the "DataBoundConstructor"
    @DataBoundConstructor
    public JenkinsRedmineBuilder(String redmineUrl, String issuesUrl, String issueFixNumberFieldId) {
        this.redmineUrl = redmineUrl;
        this.issuesUrl = issuesUrl;
        this.issueFixNumberFieldId = new Integer(issueFixNumberFieldId);
    }

    public String getRedmineUrl() {
        return redmineUrl;
    }

    public String getIssuesUrl() {
        return issuesUrl;
    }

    public Integer getIssueFixNumberFieldId() {
        return issueFixNumberFieldId;
    }

    @Override
    public boolean perform(AbstractBuild build, Launcher launcher, BuildListener listener) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream(10 * 1024 * 1024);
        Map<String, String> commitMap = new HashMap<String, String>();
        try {
            ProcStarter ps = launcher.launch();
            ps.cmds("git", "log", "--format=oneline");
            ps.pwd(build.getWorkspace());
            ps.stdout(out);
            ps.stderr(listener.getLogger());
            Proc p = ps.start();
            if (p.join() != 0) {
                return true;
            }
        } catch (IOException e) {
            listener.getLogger().println(e.toString());
            return true;
        } catch (InterruptedException e) {
            listener.getLogger().println(e.toString());
            return true;
        }

        String[] lines = out.toString().split("\n");
        for (String line1 : lines) {
            String[] line = line1.split(" ", 2);
            if (line.length != 2) {
                continue;
            }
            String commit = line[0].substring(0, 5);
            commitMap.put(commit, line[1]);
        }

        out = new ByteArrayOutputStream(10 * 1024 * 1024);
        try {
            ProcStarter ps = launcher.launch();
            ps.cmds("curl", "-H", "User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36", this.issuesUrl);
            ps.pwd(build.getWorkspace());
            ps.stdout(out);
            ps.stderr(listener.getLogger());
            Proc p = ps.start();
            if (p.join() != 0) {
                return true;
            }
        } catch (IOException e) {
            listener.getLogger().println(e.toString());
            return true;
        } catch (InterruptedException e) {
            listener.getLogger().println(e.toString());
            return true;
        }

        List issuesList = new LinkedList();
        List unknownIssuesList = new LinkedList();
        JSONObject obj = new JSONObject(out.toString());
        JSONArray issues = obj.getJSONArray("issues");
        for (int i = 0; i < issues.length(); i++) {
            Issue issue = new Issue();
            JSONObject issueJson = issues.getJSONObject(i);
            issue.setId(issueJson.getInt("id"));
            issue.setSubject(issueJson.getString("subject"));
            try {
                issue.setAssignedTo(issueJson.getJSONObject("assigned_to").getString("name"));
            } catch (JSONException e) {
                listener.getLogger().println("Issue " + issue.getId() + " has no assignee");
                continue;
            }
            try {
                JSONArray customFields = issueJson.getJSONArray("custom_fields");
                for (int j = 0; j < customFields.length(); j++) {
                    JSONObject customField = customFields.getJSONObject(j);
                    if (this.issueFixNumberFieldId.equals(customField.getInt("id"))) {
                        String commit = customField.getString("value").substring(0, 5);
                        String comment = commitMap.get(commit);
                        if (comment == null) {
                            continue;
                        }
                        issue.setComment(comment);
                        issuesList.add(issue);
                        break;
                    }
                }
            } catch (Exception e) {
                listener.getLogger().println(e.toString());
                unknownIssuesList.add(issue);
            }
        }

        build.addAction(new ResolvedIssuesAction(this.getRedmineUrl(), issuesList, unknownIssuesList));
        return true;
    }

    @Override
    public DescriptorImpl getDescriptor() {
        return (DescriptorImpl) super.getDescriptor();
    }

    @Extension
    public static final class DescriptorImpl extends BuildStepDescriptor<Builder> {

        public boolean isApplicable(Class<? extends AbstractProject> clazz) {
            return true;
        }

        public String getDisplayName() {
            return "MS Redmine Plugin";
        }
    }
}
