package co.mediasapiens.jenkinsredmine;

/**
 *
 * @author adolgarev
 */
public class Issue {

    private Integer id;
    private String subject;
    private String assignedTo;
    private String comment;

    public Issue() {
    }

    public Issue(Integer id, String subject, String assignedTo, String comment) {
        this.id = id;
        this.subject = subject;
        this.assignedTo = assignedTo;
        this.comment = comment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
