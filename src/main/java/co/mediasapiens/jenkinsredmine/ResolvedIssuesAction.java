package co.mediasapiens.jenkinsredmine;

import hudson.Functions;
import hudson.model.Action;
import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author adolgarev
 */
public class ResolvedIssuesAction implements Action, Serializable, Cloneable {
    
    private String redmineUrl;
    private Collection<Issue> issues;
    private Collection<Issue> unknownIssues;

    public ResolvedIssuesAction() {
    }

    public ResolvedIssuesAction(String issueUrlPrefix, Collection<Issue> issues, Collection<Issue> unknownIssues) {
        this.redmineUrl = issueUrlPrefix;
        this.issues = issues;
        this.unknownIssues = unknownIssues;
    }

    public String getRedmineUrl() {
        return redmineUrl;
    }

    public void setRedmineUrl(String redmineUrl) {
        this.redmineUrl = redmineUrl;
    }

    public Collection<Issue> getIssues() {
        return issues;
    }

    public void setIssues(Collection<Issue> issues) {
        this.issues = issues;
    }

    public Collection<Issue> getUnknownIssues() {
        return unknownIssues;
    }

    public void setUnknownIssues(Collection<Issue> unknownIssues) {
        this.unknownIssues = unknownIssues;
    }

    public String getIconFileName() {
        return Functions.getResourcePath() + "/plugin/JenkinsRedmine/icons/redmine_fluid_icon.png";
    }

    public String getDisplayName() {
        return "Resolved Issues";
    }

    public String getUrlName() {
        return "resolved-issues";
    }
    
}
